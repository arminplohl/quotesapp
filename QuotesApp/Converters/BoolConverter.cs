﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace QuotesApp.Converters
{
    /// <summary>
    /// Bool XAML converter
    /// </summary>
    public class BoolConverter : IValueConverter
    {
        public object TrueValue { get; set; }
        public object FalseValue { get; set; }

        public BoolConverter()
        {
            TrueValue = true;
            FalseValue = false;
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return GetFalseValue();

            if (value is bool)
            {
                if ((bool)value)
                    return GetTrueValue();
            }
            else if (value is bool?)
            {
                bool? val = (bool?)value;
                if (val.HasValue && val.Value)
                    return GetTrueValue();
            }
            else if (value is double)
            {
                if ((double)value != 0)
                    return GetTrueValue();
            }
            else if (value is float)
            {
                if ((float)value != 0)
                    return GetTrueValue();
            }
            else if (value is int)
            {
                if ((int)value != 0)
                    return GetTrueValue();
            }
            else if (value is Visibility)
            {
                if ((Visibility)value == Visibility.Visible)
                    return GetTrueValue();
            }
            else
            {
                if (!string.IsNullOrEmpty(value.ToString()))
                    return GetTrueValue();
            }

            return GetFalseValue();
        }

        object GetTrueValue()
        {
            return TrueValue;
        }

        object GetFalseValue()
        {
            return FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

}
﻿using System.Windows.Input;

namespace QuotesApp.Model
{
    public interface IDelegateCommand : ICommand
    {
        void RaiseCanExecuteChanged();
    }
}

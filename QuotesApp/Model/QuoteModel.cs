﻿using Newtonsoft.Json;

namespace QuotesApp.Model
{
    /// <summary>
    /// Quote data model class
    /// </summary>
    public class QuoteModel : BindableBase
    {
        private string quote;
        private string urlLink;

        public string Quote
        {
            get { return quote; }
            set { SetProperty(ref quote, value); }
        }

        public string UrlLink
        {
            get { return urlLink; }
            set { SetProperty(ref urlLink, value); }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public QuoteModel(){}

        /// <summary>
        /// Constructor with quote
        /// </summary>
        /// <param name="quote">Quote</param>
        public QuoteModel(string quote)
        {
            this.quote = quote;
        }

        /// <summary>
        /// Constructor with quote and url link
        /// </summary>
        /// <param name="quote">Quote</param>
        /// <param name="urlLink">Web page url</param>
        public QuoteModel(string quote, string urlLink)
        {
            this.quote = quote;
            this.urlLink = urlLink;
        }

        /// <summary>
        /// Instance method for serialization to json string
        /// </summary>
        /// <returns></returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Static method to deserialization from json string to object (QuoteModel)
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static QuoteModel FromJson(string json)
        {
            return JsonConvert.DeserializeObject<QuoteModel>(json);
        }
    }
}

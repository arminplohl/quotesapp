﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.System;
using Windows.UI.Xaml.Controls;

namespace QuotesApp.Service
{
    /// <summary>
    /// Service for app settings properties and methods
    /// </summary>
    public class AppService : IAppService
    {
        const string AppSettingsContainerKey = "applicationSettingsContainer";
        readonly ResourceLoader resourceLoader = new ResourceLoader();

        public long LastSynced
        {
            get
            {
                return GetSavedOrDefaultValue(new DateTime().Ticks);
            }

            set
            {
                if (LastSynced == value)
                    return;

                SaveValue(value);
            }
        }

        public string GetString(string key)
        {
            return resourceLoader.GetString(key);
        }

        public async Task LaunchUriAsync(Uri uri)
        {
            await Launcher.LaunchUriAsync(uri);
        }

        public async Task<ContentDialogResult> ShowDialogAsync(string title, string content, string primaryCommandText, string secondaryCommandText = null)
        {
            ContentDialog dialog = CreateDialog(title, content, primaryCommandText, secondaryCommandText);

            try
            {
                var result = await dialog.ShowAsync();
                return result;
            }
            catch
            {
                return ContentDialogResult.None;
            }
        }

        private ContentDialog CreateDialog(string title, string content, string primaryCommandText, string secondaryCommandText = null)
        {
            ContentDialog dialog = new ContentDialog();

            dialog.Title = title.ToUpper();
            dialog.Content = content;

            if (primaryCommandText != null)
            {
                dialog.IsPrimaryButtonEnabled = true;
                dialog.PrimaryButtonText = primaryCommandText;
            }

            if (secondaryCommandText != null)
            {
                dialog.IsSecondaryButtonEnabled = true;
                dialog.SecondaryButtonText = secondaryCommandText;
            }

            return dialog;
        }

        #region Helpers
        void SaveValue<T>(T value, [CallerMemberName] string key = null)
        {
            try
            {
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

                if (!localSettings.Containers.ContainsKey(AppSettingsContainerKey))
                    localSettings.CreateContainer(AppSettingsContainerKey, Windows.Storage.ApplicationDataCreateDisposition.Always);

                localSettings.Containers[AppSettingsContainerKey].Values[key] = value;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("AppSettingsService@ save value to AppSettingsService {0}", ex);
            }
        }

        T GetSavedOrDefaultValue<T>(T defaultValue, [CallerMemberName] string key = null)
        {
            T rez = defaultValue;

            try
            {
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

                if (!localSettings.Containers.ContainsKey(AppSettingsContainerKey))
                    localSettings.CreateContainer(AppSettingsContainerKey, Windows.Storage.ApplicationDataCreateDisposition.Always);

                if (localSettings.Containers[AppSettingsContainerKey].Values[key] != null)
                    return (T)localSettings.Containers[AppSettingsContainerKey].Values[key];

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("AppSettingsService@ can't get value {0}", ex));
            }

            return rez;
        }
        #endregion
    }
}

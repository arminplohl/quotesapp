﻿using System;
using System.Threading.Tasks;
using QuotesApp.Model;
using QuotesApp.Utils;

namespace QuotesApp.Service
{
    /// <summary>
    /// Dummy quotes service that returns "hardcoded" quote
    /// </summary>
    public class DummyQuoteService : IQuoteService
    {
        private readonly IAppService appService;
        private readonly IFileService fileService;

        private const string FILE_NAME = "data.json";

        public DummyQuoteService(IAppService appService, IFileService fileService)
        {
            this.appService = appService;
            this.fileService = fileService;
        }

        public async Task<QuoteModel> GetRandomQuoteAsync(bool forced = false)
        {
            DateTime lastSynced = new DateTime(appService.LastSynced);
            TimeSpan difSinceLastSync = DateTime.Now - lastSynced;

            QuoteModel model;

            double sinceLastUpdate = 0;

#if !DEBUG
            sinceLastUpdate = difSinceLastSync.TotalHours;
#else
            sinceLastUpdate = difSinceLastSync.TotalMinutes;
#endif

            if (forced || sinceLastUpdate >= 1)
            {
                await Task.Delay(3000);

                //TESTING! Hardcoded quote
                model = new QuoteModel("Don't cry because it's over, smile because it happened. ― Dr. Seuss", "http://www.goodreads.com/quotes/1173-don-t-cry-because-it-s-over-smile-because-it-happened");

                await fileService.SaveTextToFileAsync(FILE_NAME, model.ToJson());

                appService.LastSynced = DateTime.Now.Ticks;

                ShowToastAndTile(model);
            }
            else
            {
                string json = await fileService.ReadTextFromFileAsync(FILE_NAME);
                model = QuoteModel.FromJson(json);
            }

            return model;
        }

        private void ShowToastAndTile(QuoteModel model)
        {
            NotificationUtils.PopupToast(appService.GetString("NewQuoteToastTitle_"), model.Quote, string.Empty);
            TileUtils.CreateOrUpdateTile(appService.GetString("NewQuoteToastTitle_"), model.Quote);
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using Windows.Storage;

namespace QuotesApp.Service
{
    /// <summary>
    /// Service for loading and saving data from file
    /// </summary>
    public class FileService : IFileService
    {
        private StorageFolder localFolder = ApplicationData.Current.LocalFolder;

        public async Task<string> ReadTextFromFileAsync(string fileName)
        {
            StorageFile file = await GetFile(localFolder, fileName);

            string data = await FileIO.ReadTextAsync(file);

            return data;
        }

        public async Task SaveTextToFileAsync(string fileName, string data)
        {
            StorageFile file = await Create(fileName, localFolder);

            await FileIO.WriteTextAsync(file, data);
        }

        private async Task<StorageFile> GetFile(StorageFolder folder, string fileName)
        {
            return await folder.GetFileAsync(fileName);
        }

        private async Task<StorageFile> Create(string fileName, StorageFolder folder)
        {
            return await folder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace QuotesApp.Service
{
    public interface IAppService
    {
        long LastSynced { get; set; }

        Task<ContentDialogResult> ShowDialogAsync(string title, string content, string primaryCommandText, string secondaryCommandText = null);
        Task LaunchUriAsync(Uri uri);
        string GetString(string key);
    }
}

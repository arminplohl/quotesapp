﻿using QuotesApp.Model;
using System.Threading.Tasks;

namespace QuotesApp.Service
{
    public interface IQuoteService
    {
        /// <summary>
        /// Gets random quote
        /// </summary>
        /// <param name="forced">If it forced from web</param>
        /// <returns>Quotes model with quote and url link</returns>
        Task<QuoteModel> GetRandomQuoteAsync(bool forced = false);
    }
}

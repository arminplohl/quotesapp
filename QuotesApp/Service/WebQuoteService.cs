﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuotesApp.Model;
using QuotesApp.Utils;
using Windows.Web.Http;

namespace QuotesApp.Service
{
    /// <summary>
    /// Web quotes service that returns randome quote
    /// </summary>
    public class WebQuoteService : IQuoteService
    {
        private readonly IAppService appService;
        private readonly IFileService fileService;

        private const string FILE_NAME = "data.json";
        private string url;

        public WebQuoteService(string url, IAppService appService, IFileService fileService)
        {
            this.appService = appService;
            this.fileService = fileService;
            this.url = url;
        }

        public async Task<QuoteModel> GetRandomQuoteAsync(bool forced = false)
        {
            DateTime lastSynced = new DateTime(appService.LastSynced);
            TimeSpan difSinceLastSync = DateTime.Now - lastSynced;

            QuoteModel model;

            double sinceLastUpdate = 0;

#if !DEBUG
            sinceLastUpdate = difSinceLastSync.TotalHours;
#else
            sinceLastUpdate = difSinceLastSync.TotalMinutes;
#endif

            if (forced || sinceLastUpdate >= 1)
            {
                await Task.Delay(3000);

                var httpClient = new HttpClient();

                string json = await httpClient.GetStringAsync(new Uri(url));
                model = QuoteModel.FromJson(json);
                await fileService.SaveTextToFileAsync(FILE_NAME, model.ToJson());

                appService.LastSynced = DateTime.Now.Ticks;

                ShowToastAndTile(model);
            }
            else
            {
                string json = await fileService.ReadTextFromFileAsync(FILE_NAME);
                model = QuoteModel.FromJson(json);
            }

            return model;
        }

        private void ShowToastAndTile(QuoteModel model)
        {
            NotificationUtils.PopupToast(appService.GetString("NewQuoteToastTitle_"), model.Quote, string.Empty);
            TileUtils.CreateOrUpdateTile(appService.GetString("NewQuoteToastTitle_"), model.Quote);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace QuotesApp.Utils
{
    /// <summary>
    /// Helper class for toast popup
    /// </summary>
    public class NotificationUtils
    {
        public static void PopupToast(string textOne, string textTwo, string arguments)
        {
            ToastTemplateType toastTemplate = ToastTemplateType.ToastText02;
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);
            XmlNodeList textElements = toastXml.GetElementsByTagName("text");

            var element = toastXml.GetElementsByTagName("toast")[0];
            var attribute = toastXml.CreateAttribute("launch");
            attribute.NodeValue = arguments;
            element.Attributes.SetNamedItem(attribute);

            textElements[0].AppendChild(toastXml.CreateTextNode(textOne));
            textElements[1].AppendChild(toastXml.CreateTextNode(textTwo));

            DateTime deliveryTime;
#if DEBUG
            deliveryTime = DateTime.Now.AddSeconds(5);
#else
            deliveryTime = DateTime.Now.AddMinutes(1);
#endif

            ScheduledToastNotification toast = new ScheduledToastNotification(toastXml, deliveryTime);

            ToastNotificationManager.CreateToastNotifier().AddToSchedule(toast);
        }

        public static void CleanUp()
        {
            ToastNotificationManager.History.Clear();
        }
    }
}

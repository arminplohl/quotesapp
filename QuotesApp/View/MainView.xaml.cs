﻿using QuotesApp.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace QuotesApp
{
    public sealed partial class MainView : Page
    {
       private MainViewModel viewModel;

        public MainView()
        {
            this.InitializeComponent();

            viewModel = new MainViewModel(App.QuoteService, App.AppService);
            this.DataContext = viewModel;

            this.Loaded += MainView_Loaded;
            this.Unloaded += MainView_Unloaded;
        }

        private async void MainView_Loaded(object sender, RoutedEventArgs e)
        {
            await viewModel.GetData();
        }

        private void MainView_Unloaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= MainView_Loaded;
            this.Unloaded -= MainView_Unloaded;
        }
    }
}

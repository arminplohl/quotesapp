﻿using QuotesApp.Model;
using QuotesApp.Service;
using System.Threading.Tasks;
using System.Windows.Input;
using System;

namespace QuotesApp.ViewModel
{
    public class MainViewModel : BindableBase
    {
        private readonly IQuoteService quoteService;
        private readonly IAppService appService;

        private QuoteModel quoteModel;
        private bool loadingData;
        private bool linkAvailable;

        public QuoteModel QuoteModel
        {
            get { return quoteModel; }
            set { SetProperty(ref quoteModel, value); }
        }

        public bool LoadingData
        {
            get { return loadingData; }
            set { SetProperty(ref loadingData, value); }
        }

        public bool LinkAvailable
        {
            get { return linkAvailable; }
            set { SetProperty(ref linkAvailable, value); }
        }

        public ICommand ForceRefresh { get; }
        public ICommand OpenWebLink { get; }

        public MainViewModel(IQuoteService quoteService, IAppService appService)
        {
            this.quoteService = quoteService;
            this.appService = appService;

            ForceRefresh = new DelegateCommand(OnForceRefresh);
            OpenWebLink = new DelegateCommand(OnOpenWebLink);
        }

        private async void OnOpenWebLink(object obj)
        {
            string url = obj as string;
            if (!string.IsNullOrEmpty(url))
            {
                await appService.LaunchUriAsync(new Uri(url));
            }
        }

        private async void OnForceRefresh(object obj)
        {
            await GetData(true);
        }

        public async Task GetData(bool forced = false)
        {
            LoadingData = true;
            try
            {
                QuoteModel = await quoteService.GetRandomQuoteAsync(forced);
                LinkAvailable = !string.IsNullOrEmpty(QuoteModel.UrlLink);
            }
            catch (Exception ex)
            {
                await appService.ShowDialogAsync("Oooops...", ex.Message, "close");
            }
            LoadingData = false;
        }
    }
}
